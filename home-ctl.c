#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define TARGET_IOCTL 0xa0
#define POSITION_IOCTL 0xa1
#define HOME_IOCTL 0xa2
#define STOP_IOCTL 0xa3
#define START_IOCTL 0xa4

#define MAGIA_MAGIC 0xDE
#define IOW_TARGET _IOW(MAGIA_MAGIC, TARGET_IOCTL, long*)
#define IOR_POSITION _IOR(MAGIA_MAGIC, POSITION_IOCTL, long*)
#define IO_HOME _IO(MAGIA_MAGIC, HOME_IOCTL)
#define IO_STOP _IO(MAGIA_MAGIC, STOP_IOCTL)
#define IO_START _IO(MAGIA_MAGIC, START_IOCTL)

int main(int argc, char* argv[]) {
        int fd, ret;
        int32_t value, number;
        printf("**********************************\n");
        printf("*******TMC5041 test program*******\n");
        printf("*******Home motor*******\n");

        if (argc != 2) {
            printf("Usage : ioctl-example path/to/tmc5041/device");
            return -1;
        }

        printf("\nOpening Driver %s\n", argv[1]);
        fd = open(argv[1], O_RDWR);
        if(fd < 0) {
                printf("Cannot open device file...\n");
                return 0;
        }

        printf("Sending start command\n");
        ioctl(fd, IO_HOME);

        fd_set read_fds;
        FD_ZERO(&read_fds);
        FD_SET(fd, &read_fds);

        struct timeval tv;
        tv.tv_sec = 10;
        tv.tv_usec = 0;

        // Using select means we have to support poll_fds in tmc5041 driver.
        // Also while we're at it, let's finish support in ltc2904 too...
        printf("Selecting...\n");
        ret = select(fd+1, &read_fds, NULL, NULL, &tv);

        if (-1 == ret) {
            return EXIT_FAILURE;
        }

        if (0 == ret) {
            // Shall we start again here, but diminishing the timeout value ?
        }

        if (FD_ISSET(fd, &read_fds)) {
            printf("Homing complete !\n");
        } else {
            printf("Problem\n");
        }

        printf("Command sent, sleeping for 10sec\n");
        sleep(5);

        printf("Sending stop command\n");
        ioctl(fd, IO_STOP);
        printf("Command sent\n");

        printf("Closing Driver\n");
        close(fd);
}
