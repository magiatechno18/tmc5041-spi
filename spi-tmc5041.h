#ifndef TMC5041IOCTL_H
#define TMC5041IOCTL_H

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/ioctl.h>
#include <linux/spi/spi.h>
#include <linux/miscdevice.h>
#include <linux/gpio/consumer.h>
#include <linux/wait.h>
#include <linux/poll.h>
#include "TMC5041_defines.h"

struct tmc5041;

/**
 * @brief The Payload union
 */
union Payload {
    uint8_t array[4];
    uint32_t data;
};

/**
 * @brief The tmc5041_platform_data struct stores the data parsed from the device tree
 */
struct tmc5041_platform_data {

};

typedef int (*register_check_callback)(struct tmc5041*, int*);

struct tmc5041_hardware {
    bool homing_procedure_active;
    bool homing_procedure_complete;
    bool limit_switches_set_up;
    uint8_t homing_checks_counter;
    uint8_t position_checks_counter;
    enum tmc5041_hardware_status hw_status;
//    bool looping;
    uint8_t ramp_mode;
    char step_size;

    uint8_t register_to_check;
//    uint32_t mask_to_check;
//    uint8_t shift_to_check;
    uint32_t register_value;
    register_check_callback check_callback;
    bool register_check_require_wakeup;
    bool using_second_channel;
};

/**
 * @brief The tmc5041 struct holds the driver's private data
 */
struct tmc5041 {
    /* driver pointers */
    struct tmc5041_platform_data *platform_data;
    struct spi_device *spi_device;
    struct miscdevice misc_device;
    struct gpio_desc *gpio_enable;
    struct gpio_desc *gpio_power;

    struct workqueue_struct *tmc5041_wq;
    struct delayed_work homing_check_work;
    struct delayed_work register_check_work;
//    struct delayed_work change_direction_work;

    wait_queue_head_t data_ready_queue;

    struct tmc5041_hardware hardware;

    bool powered;
};

static int tmc5041_spi_fdx_transfer(struct spi_device*, const uint8_t reg, const uint32_t data, uint8_t* status, uint32_t* response, bool write_operation);

static int tmc5041_position_check_callback(struct tmc5041*, int*);

static int tmc5041_w_target(int, struct tmc5041*);
static int tmc5041_r_position(int*, struct tmc5041*);
static int tmc5041_home(struct tmc5041*);
static int tmc5041_stop(struct tmc5041*);
static int tmc5041_start(struct tmc5041*);
static int tmc5041_setup(struct tmc5041*);

static int tmc5041_fopen(struct inode*, struct file*);
static int tmc5041_frelease(struct inode*, struct file*);
static unsigned int tmc5041_fpoll(struct file*, poll_table *);
static int tmc5041_fread(struct file*, char __user *buf, size_t len, loff_t *off);
static int tmc5041_fwrite(struct file*, const char *buf, size_t len, loff_t *off);
static long tmc5041_fioctl(struct file*, unsigned int, unsigned long);
static inline struct tmc5041* to_tmc5041_priv(struct file* file);

static int tmc5041_probe(struct spi_device*);
static int tmc5041_remove(struct spi_device*);
//static struct tmc5041_platform_data* tmc5041_parse_dt(struct spi_device*);

#endif // TMC5041IOCTL_H
