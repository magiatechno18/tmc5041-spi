#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#define TARGET_IOCTL 0xa0
#define POSITION_IOCTL 0xa1
#define HOME_IOCTL 0xa2
#define STOP_IOCTL 0xa3
#define START_IOCTL 0xa4
#define LOOP_IOCTL 0xa5
#define SWAP_CHANNEL_IOCTL 0xa7

#define MAGIA_MAGIC 0xDE
#define IOW_TARGET _IOW(MAGIA_MAGIC, TARGET_IOCTL, long*)
#define IOR_POSITION _IOR(MAGIA_MAGIC, POSITION_IOCTL, long*)
#define IO_HOME _IO(MAGIA_MAGIC, HOME_IOCTL)
#define IO_STOP _IO(MAGIA_MAGIC, STOP_IOCTL)
#define IO_START _IO(MAGIA_MAGIC, START_IOCTL)
#define IO_LOOP _IO(MAGIA_MAGIC, LOOP_IOCTL)
#define IOW_SWAP_CHANNEL _IOW(MAGIA_MAGIC, SWAP_CHANNEL_IOCTL, long*)

int main(int argc, char* argv[]) {
        int fd;
        int32_t value, number;
        printf("**********************************\n");
        printf("*******TMC5041 test program*******\n");
        printf("*******Loop motor*******\n");

        if (argc != 3) {
            printf("Usage : ioctl-example path/to/tmc5041/device <channel> (1 or 2)");
            return -1;
        }

        printf("\nOpening Driver %s\n", argv[1]);
        fd = open(argv[1], O_RDWR);
        if(fd < 0) {
                printf("Cannot open device file...\n");
                return 0;
        }

	value = atoi(argv[2]);
	if (value != 1 && value != 2) {
		printf("Wrong channel provided : %i\n", value);
		return 0;
	} 

	value--;
/*
        printf("Sending start command\n");
        ioctl(fd, IO_LOOP);


        printf("Command sent, sleeping for 10sec\n");
        sleep(15);
*/
        printf("Sending channel switch command\n");
        ioctl(fd, IOW_SWAP_CHANNEL, &value);
        printf("Command sent\n");


        printf("Closing Driver\n");
        close(fd);
}
