TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        helloworld_class_driver.c \
    home-ctl.c \
        main.c \
        spi-tmc5041.c \
    target-ctl.c

HEADERS += \
    spi-tmc5041.h

INCLUDEPATH += \
    ../oe-build/yocto-ktn/build-magia/tmp-glibc/work-shared/40099086/kernel-source/include \
    /opt/magia/magnet_1.0/sysroots/armv7ahf-neon-kontron-linux-gnueabi/usr/include \

