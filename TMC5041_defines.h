#ifndef TMC5041_DEFINES_H
#define TMC5041_DEFINES_H

#define BUF_BYTE_LENGTH 5

#define TARGET_IOCTL 0xa0
#define POSITION_IOCTL 0xa1
#define HOME_IOCTL 0xa2
#define STOP_IOCTL 0xa3
#define START_IOCTL 0xa4
//#define LOOP_IOCTL 0xa5
#define STATUS_IOCTL 0xa6
#define SWAP_CHANNEL_IOCTL 0xa7

#define MAGIA_MAGIC 0xDE
#define IOW_TARGET _IOW(MAGIA_MAGIC, TARGET_IOCTL, int*)
#define IOR_POSITION _IOR(MAGIA_MAGIC, POSITION_IOCTL, int*)
#define IO_HOME _IO(MAGIA_MAGIC, HOME_IOCTL)
#define IO_STOP _IO(MAGIA_MAGIC, STOP_IOCTL)
#define IO_START _IO(MAGIA_MAGIC, START_IOCTL)
//#define IO_LOOP _IO(MAGIA_MAGIC, LOOP_IOCTL)
#define IOR_STATUS _IOR(MAGIA_MAGIC, STATUS_IOCTL, int*)
#define IOW_SWAP_CHANNEL _IOW(MAGIA_MAGIC, SWAP_CHANNEL_IOCTL, int*)

#define MISC_NAME "tmc5041-misc"

// ----- registers definition ----- //

// General configuration
#define GCONF 0x00
#define GSTAT 0x01
#define TEST_SEL 0x03
#define TMC_INPUT 0x04
#define X_COMPARE 0x05

// Ramp generation motion control
// These controls are for motor 1. Add 0x20 to the address to access motor 2 (addresses 0x40...0x4D)
#define RAMP_MOT_2(reg) (reg + 0x20)
#define RAMPMODE 0x20
#define XACTUAL 0x21
#define VACTUAL 0x22
#define VSTART 0x23
#define A1 0x24
#define V1 0x25
#define AMAX 0x26
#define VMAX 0x27
#define DMAX 0x28
#define D1 0x2A
#define VSTOP 0x2B
#define TZEROWAIT 0x2C
#define XTARGET 0x2D

// Ramp generation driver features control
// These controls are for motor 1. Add 0x20 to the address to access motor 2 (addresses 0x50...0x5D)
#define IHOLDRUN 0x30
#define VCOOLTHRS 0x31
#define VHIGH 0x32
#define SW_MODE 0x34
#define RAMP_STAT 0x35
#define XLATCH 0x36

// Microstep
// Same controls for both motors
#define MSLUT0 0x60
#define MSLUT1 0x61
#define MSLUT2 0x62
#define MSLUT3 0x63
#define MSLUT4 0x64
#define MSLUT5 0x65
#define MSLUT6 0x66
#define MSLUT7 0x67
#define MSLUTSEL 0x68
#define MSLUTSTART 0x69

// Motor driver
// These controls are for motor 1. Add 0x10 to the address to access motor 2 (addresses 0x7A...0x7F)
#define REG_MOT_2(reg) (reg + 0x10)
#define MSCNT 0x6A
#define MSCURACT 0x6B
#define CHOPCONF 0x6C
#define COOLCONF 0x6D
#define DRV_STATUS 0x6F

// Voltage PWM mode StealthChop
// These controls are for motor 1. Add 0x08 to the address to access motor 2 (addresses 0x18...0x1F)
#define VOLT_MOT_2(reg) (reg + 0x08)
#define PWMCONF 0x10
#define PWM_STATUS 0x11

// RAMPMODES
#define MODE_POSITION 0
#define MODE_VELPOS 1
#define MODE_VELNEG 2
#define MODE_HOLD 3

#define UNKNOWN_REGISTER 0xFF

enum tmc5041_hardware_status {
    NO_ERROR,
    completed_tasks,
    HOMING_COMPLETE,
    TARGET_POSITION_REACHED,
    OPERATION_COMPLETE,
    command_issued,
    START_COMMAND_ISSUED,
    STOP_COMMAND_ISSUED,
    HOME_COMMAND_ISSUED,
    TARGET_COMMAND_ISSUED,
    POSITION_READ_COMMAND_ISSUED,
    //LOOP_COMMAND_ISSUED,
    progress,
    HOMING_IN_PROGRESS,
    TARGET_IN_PROGRESS,
    //LOOP_IN_PROGRESS,
    OPERATION_IN_PROGRESS,
    errors,
    TIMEOUT_ERROR,
    REGISTER_CHECK_CONFIG_ERROR,
    COMMUNICATION_ERROR,
    MOVEMENT_PREREQUISITES_ERROR,
    BAD_POLL_CALL_ERROR,
    //LOOP_BAD_CONFIG_ERROR,
    HOMING_CHECK_COUNT_EXCEEDED_ERROR,
    POSITION_CHECK_COUNT_EXCEEDED_ERROR,
    NO_POWER_ERROR,
    HOMING_STOPPED,
    LAST_ERROR,
};

#endif // TMC5041_DEFINES_HPP
