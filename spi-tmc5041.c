#include "spi-tmc5041.h"
#include "TMC5041_Fields.h"
#include <linux/spi/spi.h>
#include <linux/byteorder/generic.h>
#include <linux/delay.h>

#include "tmc5041_driver_version.h"

#define FIELD_GET(data, mask, shift) \
    (((data) & (mask)) >> (shift))
#define FIELD_SET(data, mask, shift, value) \
    (((data) & (~(mask))) | (((value) << (shift)) & (mask)))
    
#define TMC5041_WRITE true
#define TMC5041_READ false

#define MAX_HOMING_CHECKS 25
#define MAX_POSITION_CHECKS 20
#define MAX_HOME_SENSORS_CHECK 4

// As HZ = 100, a jiffy is 1/100s
#define POLL_1S 100
#define POLL_500MS 50
#define POLL_100MS 10

#define NO_SPEED 0
#define LOW_SPEED 25000
#define MED_SPEED 50000
#define HIGH_SPEED 112500
#define VERY_HIGH_SPEED 225000

#define SECOND_CHANNEL_LOW_SPEED 5000
#define SECOND_CHANNEL_MED_SPEED 20000
#define SECOND_CHANNEL_HIGH_SPEED 5000

#define LOW_ACCEL 1000
#define MED_ACCEL 2500
#define HIGH_ACCEL 20000
#define MED_HIGH_ACCEL 30000
#define VERY_HIGH_ACCEL 40000

#define SECOND_CHANNEL_LOW_ACCEL 200
#define SECOND_CHANNEL_MED_ACCEL 500
#define SECOND_CHANNEL_HIGH_ACCEL 1000

#define FIRST_CHANNEL_SPEED VERY_HIGH_SPEED
#define FIRST_CHANNEL_HOMING_SPEED HIGH_SPEED
#define FIRST_CHANNEL_ACCEL HIGH_ACCEL
#define SECOND_CHANNEL_SPEED SECOND_CHANNEL_HIGH_SPEED
#define SECOND_CHANNEL_ACCEL SECOND_CHANNEL_LOW_ACCEL

#define FIRST_CHANNEL_CHOPCONF 0x000100C5
#define SECOND_CHANNEL_CHOPCONF 0x000100C5

#define FIRST_CHANNEL_IHOLDRUN 0x00011F01
#define SECOND_CHANNEL_IHOLDRUN 0x00011F01

#define TZEROWAIT_CONTENT 0x00002710

#define FIRST_CHANNEL_PWMCONF 0x000401C8
#define SECOND_CHANNEL_PWMCONF 0x000401C8

#define VHIGH_CONTENT 0x00061A80

#define VCOOLTHRS_CONTENT 0x00007530

#define VSTART_CONTENT 0x00000005

#define VSTOP_CONTENT 0x00000010

#define V1_CONTENT 0x00000000

static uint8_t tmc5041_reg_shift_for_second_channel(struct tmc5041* chip, const uint8_t reg) {
    _Bool shift = chip->hardware.using_second_channel;
    switch (reg) {
    case GCONF ... X_COMPARE:
        return 0x00;
    case RAMPMODE ... XTARGET:
        return shift ? 0x20 : 0x00;
    case IHOLDRUN ... XLATCH:
        return shift ? 0x20 : 0x00;
    case MSLUT0 ... MSLUTSTART:
        return 0x00;
    case MSCNT ... DRV_STATUS:
        return shift ? 0x10 : 0x00;
    case PWMCONF ... PWM_STATUS:
        return shift ? 0x08 : 0x00;
    default:
        dev_err(&chip->spi_device->dev, "Unknown register !!\n");
        return 0xFF;
    }
}

/**
 * @brief tmc5041_spi_fdx_transfer
 * @param spi
 * @param reg
 * @param data
 * @param status
 * @param response
 * @param write_operation
 * @return
 * Inspired from ili922x.c
 */
static int tmc5041_spi_fdx_transfer(struct spi_device *spi, const uint8_t reg, const uint32_t data, uint8_t* status, uint32_t* response, bool write_operation) {
    struct spi_message msg;
    struct spi_transfer xfer;
    uint8_t* tbuf = NULL;
    uint8_t* rbuf = NULL;
    int ret = 0;
    union Payload pl;
    struct tmc5041* chip;
    uint8_t reg_shift = 0xFF;

    chip = spi_get_drvdata(spi);
    reg_shift = tmc5041_reg_shift_for_second_channel(chip, reg);

    /* If there was a problem with the definition of reg_shift, we catch it here. */
    if (reg_shift == 0xFF) {
        printk(KERN_ERR "Cannot use a register shift of 0xFF\n");
        return -EINVAL;
    }

    tbuf = (uint8_t*) kzalloc(BUF_BYTE_LENGTH * sizeof (uint8_t), GFP_KERNEL);
    if (!tbuf) {
        printk(KERN_ERR "Couldn't allocate memory for Tx buffer\n");
        return -ENOMEM;
    }
    rbuf = (uint8_t*) kzalloc(BUF_BYTE_LENGTH * sizeof (uint8_t), GFP_KERNEL);
    if (!rbuf) {
        printk(KERN_ERR "Couldn't allocate memory for Rx buffer\n");
        kfree(tbuf);
        return -ENOMEM;
    }

    pl.data = cpu_to_be32(data);

    memset(&xfer, 0, sizeof(struct spi_transfer));

    spi_message_init(&msg);
    xfer.tx_buf = tbuf;
    xfer.rx_buf = rbuf;
    xfer.cs_change = 0; // See https://github.com/beagleboard/kernel/issues/85#issuecomment-32304365

    tbuf[0] = reg + reg_shift;
    tbuf[1] = pl.array[0];
    tbuf[2] = pl.array[1];
    tbuf[3] = pl.array[2];
    tbuf[4] = pl.array[3];

    if (write_operation) {
        tbuf[0] = tbuf[0] | 0x80; // Setting the MSB to 1 for write access.
    }

    xfer.bits_per_word = 8;
    xfer.len = BUF_BYTE_LENGTH;

    spi_message_add_tail(&xfer, &msg);

    dev_info(&spi->dev, "sending payload : 0x%02x:%02x:%02x:%02x:%02x\n", tbuf[0], tbuf[1], tbuf[2], tbuf[3], tbuf[4]);

    ret = spi_sync(spi, &msg);
    if (ret < 0) {
        dev_err(&spi->dev, "%s: error sending SPI message 0x%x\n", __func__, ret);
        kfree(tbuf);
        kfree(rbuf);
        return ret;
    }

    *status = rbuf[0];
    pl.array[0] = rbuf[1];
    pl.array[1] = rbuf[2];
    pl.array[2] = rbuf[3];
    pl.array[3] = rbuf[4];
    *response = be32_to_cpu(pl.data);

    dev_info(&spi->dev, "TMC5041 answer : 0x%02x:%02x:%02x:%02x:%02x\n", rbuf[0], rbuf[1], rbuf[2], rbuf[3], rbuf[4]);

    kfree(tbuf);
    kfree(rbuf);

    return 0;
}

/**
 * @brief tmc5041_w_target is called by the IOW_TARGET ioctl. It will schedule a call to register_check_work, with
 * the register set to ??? in order to check regularly if we have reached the target position.
 * This one will not wake up anything, but the scheduled work will.
 * While in this operation, status is set to TARGET_IN_PROGRESS
 * @param arg
 * @return
 */
static int tmc5041_w_target(int target, struct tmc5041* chip) {
    struct spi_device* spi = chip->spi_device;
    uint8_t status;
    uint32_t response;
    uint32_t setting;
    int ret = 0;

    printk(KERN_INFO "TMC5041 target \n");
    
    chip->hardware.hw_status = TARGET_IN_PROGRESS;

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered\n");
        chip->hardware.hw_status = NO_POWER_ERROR;
        return -EINVAL;
    }
    
    // The limit switches should be set up.
    // The homing should have been done.
    if (!chip->hardware.limit_switches_set_up || !chip->hardware.homing_procedure_complete) {
        dev_err(&spi->dev, "Homing procedure not complete, or limit switches not set. Target operation aborted\n");
        chip->hardware.hw_status = MOVEMENT_PREREQUISITES_ERROR;
        //return -EINVAL;
    }
    
    ret = tmc5041_spi_fdx_transfer(spi, RAMPMODE, MODE_POSITION, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set rampmode\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_SPEED : FIRST_CHANNEL_SPEED;
    ret = tmc5041_spi_fdx_transfer(spi, VMAX, setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VMAX\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    target = chip->hardware.using_second_channel ? target : target;
    ret = tmc5041_spi_fdx_transfer(spi, XTARGET, target, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set rampmode\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }


    chip->hardware.register_to_check = RAMP_STAT;
    chip->hardware.check_callback = tmc5041_position_check_callback;
    chip->hardware.position_checks_counter = 0;
    // We are assuming that the caller will then sleep on a poll or select.
    // Therefore, the callback should wake the process up in case of success / error
    chip->hardware.register_check_require_wakeup = true;
    queue_delayed_work(chip->tmc5041_wq, &chip->register_check_work, POLL_1S);

    return ret;
}

/**
 * @brief tmc5041_r_position TODO, doesn't have anything to wake
 * @param arg
 * @return
 */
static int tmc5041_r_position(int* position, struct tmc5041* chip) {
    printk(KERN_INFO "TMC5041 position \n");

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered\n");
        chip->hardware.hw_status = NO_POWER_ERROR;
        return -EINVAL;
    }
    
    chip->hardware.hw_status = NO_ERROR; // TO check

    return 0;
}

/**
 * @brief tmc5041_setup_limit_switches has nothing to wake
 * It's also never called by itself, so it doesn't have to affect the hw_status variable, except for errors.
 * @param chip
 * @return
 */
static int tmc5041_setup_limit_switches(struct tmc5041* chip) {
    uint8_t status;
    uint32_t response;
    uint32_t command = 0;
    int ret = 0;
    struct spi_device* spi = chip->spi_device;

    printk(KERN_INFO "TMC5041 setup limit switches \n");
    if (chip->hardware.limit_switches_set_up)
        dev_info(&spi->dev, "Switches were already setup\n");
    chip->hardware.limit_switches_set_up = false;

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered\n");
        chip->hardware.hw_status = NO_POWER_ERROR;
        return -EINVAL;
    }
    
    /* Setup register SW_MODE
     * - 11 -> 1 to enable soft stop
     * -  7 -> 1 to enable position latch on active right switch edge
     * -  5 -> 1 to enable position latch on active left switch edge
     * -  3 -> 0 to set right switch high active for first channel. set to 1 for low active for second channel.
     * -  2 -> 0 to set left switch high active for first channel. set to 1 for low active for second channel.
     * -  1 -> 1 to set right stop enable
     * -  0 -> 1 to set left stop enable
     * To set fields : FIELD_SET(data, mask, shift, value)
     */
    
    ret = tmc5041_spi_fdx_transfer(spi, SW_MODE, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }

    ret = tmc5041_spi_fdx_transfer(spi, SW_MODE, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
        
    command = response; // Save the previous settings, and change only the bits we want
    command = FIELD_SET(command, TMC5041_EN_SOFTSTOP_MASK, TMC5041_EN_SOFTSTOP_SHIFT, 1);
    command = FIELD_SET(command, TMC5041_LATCH_L_ACTIVE_MASK, TMC5041_LATCH_L_ACTIVE_SHIFT, 1);
    command = FIELD_SET(command, TMC5041_LATCH_R_ACTIVE_MASK, TMC5041_LATCH_R_ACTIVE_SHIFT, 1);
    command = FIELD_SET(command, TMC5041_POL_STOP_R_MASK, TMC5041_POL_STOP_R_SHIFT, chip->hardware.using_second_channel ? 0 : 0);
    command = FIELD_SET(command, TMC5041_POL_STOP_L_MASK, TMC5041_POL_STOP_L_SHIFT, chip->hardware.using_second_channel ? 1 : 0);
    command = FIELD_SET(command, TMC5041_STOP_R_ENABLE_MASK, TMC5041_STOP_R_ENABLE_SHIFT, 1);
    command = FIELD_SET(command, TMC5041_STOP_L_ENABLE_MASK, TMC5041_STOP_L_ENABLE_SHIFT, 1);
    command = FIELD_SET(command, TMC5041_SWAP_LR_MASK, TMC5041_SWAP_LR_SHIFT, chip->hardware.using_second_channel ? 0 : 0);
    
    ret = tmc5041_spi_fdx_transfer(spi, SW_MODE, command, &status, &response, TMC5041_WRITE);
    if (ret < 0)
        chip->hardware.hw_status = COMMUNICATION_ERROR;
    else {
        chip->hardware.limit_switches_set_up = true;
    }
    return ret;
}

/**
 * @brief tmc5041_home is called when IO_HOME ioctl is selected.
 * This method's return value is directly transmitted to the caller, so there should be no process waiting...
 * The home check, however, will wake up any waiting process.
 * @param arg
 * @return
 */
static int tmc5041_home(struct tmc5041* chip) {
    uint8_t status;
    uint32_t response;
    uint32_t command;
    uint32_t setting;
    int ret;
    int tries = MAX_HOME_SENSORS_CHECK;
    struct spi_device* spi = chip->spi_device;

    printk(KERN_INFO "TMC5041 home \n");
    chip->hardware.hw_status = HOMING_IN_PROGRESS;

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered\n");
        chip->hardware.hw_status = NO_POWER_ERROR;
        return -EINVAL;
    }
    
home_sensor_status_check:
    if (tries-- < 0) {
        dev_alert(&spi->dev, "Wasn't able to get away from home switch. Potential hardware failure");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return -EINVAL;
    }

    /* TMC5041 homing procedure */
    /* 1 - Read sensors, just in case. If we're already touching, move away. */
    command = 0;
    ret = tmc5041_spi_fdx_transfer(spi, RAMP_STAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    // This command must be sent twice because the chip doesn't send back answers on the first call.
    ret = tmc5041_spi_fdx_transfer(spi, RAMP_STAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
        
    if (FIELD_GET(response, TMC5041_STATUS_STOP_R_MASK, TMC5041_STATUS_STOP_R_SHIFT))
        dev_info(&spi->dev, "Right switch active");
    else if (FIELD_GET(response, TMC5041_STATUS_STOP_L_MASK, TMC5041_STATUS_STOP_L_SHIFT))
        dev_info(&spi->dev, "Left switch active");
        
    // Move away from the right switch, as it is our target - CHECK IF IT'S REALLY NECESSARY
    if (FIELD_GET(response, TMC5041_STATUS_STOP_R_MASK, TMC5041_STATUS_STOP_R_SHIFT)) {
        setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_SPEED : FIRST_CHANNEL_SPEED;
        ret = tmc5041_spi_fdx_transfer(spi, VMAX, setting, &status, &response, TMC5041_WRITE);
        if (ret < 0) {
            dev_err(&spi->dev, "Couldn't set up VMAX\n");
            chip->hardware.hw_status = COMMUNICATION_ERROR;
            return ret;
        }
        setting = chip->hardware.using_second_channel ? MODE_VELNEG : MODE_VELNEG;
        ret = tmc5041_spi_fdx_transfer(spi, RAMPMODE, setting, &status, &response, TMC5041_WRITE);
        if (ret < 0) {
            dev_err(&spi->dev, "Couldn't set up RAMPMODE\n");
            chip->hardware.hw_status = COMMUNICATION_ERROR;
            return ret;
        }

        usleep_range(0.9e6, 1.1e6); // Wait for approx 1s, the tray should move away.

        ret = tmc5041_stop(chip); // hw_status will be set as needed.
        if (ret < 0) {
            dev_err(&spi->dev, "ERROR Couldn't stop the motor !!\n");
            return ret;
        }
        
        goto home_sensor_status_check;
    }
    
    /* 2 - Setup the limit switches - normally it's been done by the setup */
    
    ret = tmc5041_setup_limit_switches(chip);
    if (ret < 0) // No need to set hw_status, it's already set by setup_limit_switches.
        return ret;
     
    /* 3 - Start moving in the direction of the target switch */
    /* Hypothesis : right is positive, left is negative */
    /* We want a right stop homing, so let's start moving in the positives */

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_SPEED : FIRST_CHANNEL_HOMING_SPEED;
    ret = tmc5041_spi_fdx_transfer(spi, VMAX, setting, &status, &response, TMC5041_WRITE); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VMAX\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }

    setting = chip->hardware.using_second_channel ? MODE_VELPOS : MODE_VELPOS;
    ret = tmc5041_spi_fdx_transfer(spi, RAMPMODE, setting, &status, &response, TMC5041_WRITE); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up RAMPMODE\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    /* 4 - Once the motor has touched, it will stop. Poll VACTUAL to check current velocity = 0 */
    /* However, we don't want just to wait in here. Let's schedule a poll and return from this method. */
    /* Steps 5 and 6 will be run from this scheduled callback, when velocity = 0 */
    /* This callback will reschedule itself up to MAX_HOMING_CHECKS times */
    
    dev_info(&spi->dev, "Hardware status was %i", chip->hardware.hw_status);
    chip->hardware.hw_status = HOMING_IN_PROGRESS;
    chip->hardware.homing_checks_counter = 0;
    chip->hardware.homing_procedure_active = true;
    queue_delayed_work(chip->tmc5041_wq, &chip->homing_check_work, POLL_1S);

    return ret;
}

/**
 * @brief tmc5041_home_completion should be called by home_poll_work once the velocity
 * of the motor is at 0. Once this condition is met, we set the homing position to 0 according
 * to the TMC5041 datasheet (XLATCH - XACTUAL). Maybe we should also check the status of the
 * switch ? Just to be sure that we haven't just completely blocked against something...
 * Anyways, this method should not wake up any sleeping processes.
 * @param chip
 * @return an error code to the caller, which should be home_poll_work.
 */
static int tmc5041_home_completion(struct tmc5041* chip) {
    struct spi_device* spi = chip->spi_device;
    uint8_t status;
    uint32_t response;
    uint32_t command;
    int ret = 0;
    
    /* 5.0 are we actually touching a switch ? */
    ret = tmc5041_spi_fdx_transfer(spi, RAMP_STAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    // This command must be sent twice because the chip doesn't send back answers on the first call.
    ret = tmc5041_spi_fdx_transfer(spi, RAMP_STAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
        
    if (FIELD_GET(response, TMC5041_STATUS_STOP_R_MASK, TMC5041_STATUS_STOP_R_SHIFT))
        dev_info(&spi->dev, "Right switch active");
    else if (FIELD_GET(response, TMC5041_STATUS_STOP_L_MASK, TMC5041_STATUS_STOP_L_SHIFT))
        dev_info(&spi->dev, "Left switch active");
        
    dev_info(&spi->dev, "Switch check done ^");

    uint32_t xlatch, xactual;
    /* 5 - Once the motor is stopped, switch the ramp mode to hold and calculate the difference
     * between latched position and current position. */
    ret = tmc5041_spi_fdx_transfer(spi, RAMPMODE, MODE_HOLD, &status, &response, TMC5041_WRITE); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set RAMPMODE\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    command = 0;
    ret = tmc5041_spi_fdx_transfer(spi, XLATCH, command, &status, &response, TMC5041_READ); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't read XLATCH - 1\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    ret = tmc5041_spi_fdx_transfer(spi, XACTUAL, command, &status, &response, TMC5041_READ); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't read XLATCH - XACTUAL\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    xlatch = response;
    ret = tmc5041_spi_fdx_transfer(spi, XACTUAL, command, &status, &response, TMC5041_READ); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't read XACTUAL - 2\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    xactual = response;
    
    /* 6 - Write the calculated difference in the actual position register. Moving then to 0 will 
     * bring the motor to the switching point. */
     
    ret = tmc5041_spi_fdx_transfer(spi, XACTUAL, (uint32_t) (xactual - xlatch), &status, &response, TMC5041_WRITE); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set XACTUAL\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }

    /* 7 - Stop the motor by writing to VMAX and RAMPMODE */
    ret = tmc5041_stop(chip);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't stop the motor correctly\n");
        return ret;
    }
    
    dev_info(&spi->dev, "Homing complete\n");
    chip->hardware.hw_status = HOMING_COMPLETE;
    chip->hardware.homing_procedure_complete = true;
    
    // We could wake up any sleeping process here, but as home completion is called by is_home_poll_work
    // let's do it over there.

    return ret;
}

/**
 * @brief tmc5041_is_home_poll_work is scheduled to run after 1S by tmc5041_home, and can reschedule itself
 * if certains conditions are met, mainly if we aren't home and we haven't exceeded the home check count.
 * Otherwise, it will skip the reschedule and wake up any process that may be sleeping after calling select or poll
 * on the file descriptor.
 * @param work_struct_ptr
 */
static void tmc5041_is_home_poll_work(struct work_struct *work_struct_ptr) {
    uint8_t status;
    uint32_t response;
    uint32_t command = 0;
    int ret;
    int velocity;
    int switch_status = 0;
    
    printk(KERN_INFO "TMC5041 is home poll\n");

    struct delayed_work* delayed = container_of(work_struct_ptr, struct delayed_work, work);
    struct tmc5041* chip = container_of(delayed, struct tmc5041, homing_check_work);
    struct spi_device* spi = chip->spi_device;
    
    if (!chip->powered) {
        dev_err(&spi->dev, "TMC5041 is not powered\n");
        chip->hardware.hw_status = NO_POWER_ERROR;
        goto signal_status;
    }

    if (OPERATION_COMPLETE == chip->hardware.hw_status) {
        // The operation was stopped manually. No need to continue polling.
        dev_info(&chip->spi_device->dev, "Operation stopped manually. Not polling anymore\n");
        chip->hardware.hw_status = HOMING_STOPPED;
        goto signal_status;
    }

    if (!chip->hardware.homing_procedure_active) {
        dev_err(&spi->dev, "Called home poll work but homing_procedure_active flag is set to false\n");
        chip->hardware.hw_status = BAD_POLL_CALL_ERROR;
        goto signal_status;
    }
    
    // Increment the homing check counter, to use it as a pseudo timeout
    chip->hardware.homing_checks_counter++;

    /* 5.0 are we actually touching a switch ? */
    ret = tmc5041_spi_fdx_transfer(spi, RAMP_STAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    
    // This command must be sent twice because the chip doesn't send back answers on the first call.
    ret = tmc5041_spi_fdx_transfer(spi, RAMP_STAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
        
    if (FIELD_GET(response, TMC5041_STATUS_STOP_R_MASK, TMC5041_STATUS_STOP_R_SHIFT))
        dev_info(&spi->dev, "Right switch active");
    else if (FIELD_GET(response, TMC5041_STATUS_STOP_L_MASK, TMC5041_STATUS_STOP_L_SHIFT))
        dev_info(&spi->dev, "Left switch active");
        
    dev_info(&spi->dev, "Switch check done ^");

    switch_status = FIELD_GET(response, TMC5041_STATUS_STOP_L_MASK, TMC5041_STATUS_STOP_L_SHIFT) | FIELD_GET(response, TMC5041_STATUS_STOP_R_MASK, TMC5041_STATUS_STOP_R_SHIFT);
    
    command = 0;
    ret = tmc5041_spi_fdx_transfer(spi, VACTUAL, command, &status, &response, TMC5041_READ); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't read VACTUAL - first call\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto signal_status;
    }
    
    ret = tmc5041_spi_fdx_transfer(spi, VACTUAL, command, &status, &response, TMC5041_READ); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't read VACTUAL - first call\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto signal_status;
    }
    
    velocity = FIELD_GET(response, TMC5041_VACTUAL_MASK, TMC5041_VACTUAL_SHIFT);
    
    if (0 == velocity && switch_status != 0) {
        dev_info(&spi->dev, "Velocity is zero and a switch is triggered\n");
        ret = tmc5041_home_completion(chip);
        
        // If ret < 0, the status should already be set by home_completion. Let's go
        // directly to the status signaling without rescheduling.
        // Actually, whatever the return of this method, we should go to no_reschedule,
        // so let's remove the following commands.
        //if (ret < 0)
        //    goto no_reschedule;
        
        // We don't have to reschedule, but we have to inform userspace that homing is finished
        // Technically the hw_status update is already done by home_completion, so let's remove the
        // following command.
        //chip->hardware.hw_status = HOMING_COMPLETE; 

        chip->hardware.homing_procedure_active = false;

        goto no_reschedule;
        
    } else {
        dev_info(&spi->dev, "Velocity is %i\n", velocity);
        if (chip->hardware.homing_checks_counter > MAX_HOMING_CHECKS) {
            // There is a problem with the motor, as we have exceeded the pseudo timeout
            // on the home checks.
            dev_warn(&spi->dev, "Exceeded maximum number of checks for homing procedure\n");
            
            // We won't reschedule the poll, and we'll set the corresponding error.
            chip->hardware.hw_status = HOMING_CHECK_COUNT_EXCEEDED_ERROR;
            goto no_reschedule; 
        }
        
        // Just a sanity check, we should still have hw_status == OPERATION_IN_PROGRESS
        // Investigation required if the following assertion fails.
        if (chip->hardware.hw_status != HOMING_IN_PROGRESS)
            dev_warn(&spi->dev, "WARNING, hw_status should be OPERATION_IN_PROGRESS (home_poll)\n");
        
        // We didn't exceed the max number of checks, let's reschedule.
        dev_info(&spi->dev, "Rescheduling\n");
        queue_delayed_work(chip->tmc5041_wq, &chip->homing_check_work, POLL_1S);
        return;
    }
    
no_reschedule:
    // Destroy work queue ? At least don't reschedule
signal_status:
    // Got to use the misc device to signal errors
_wake_up_and_return:
    // AND wake up any waiting process here !!
    // And the next time the file is read, the hw_status will be whatever was set by the last instruction before this block.
    wake_up_interruptible(&chip->data_ready_queue);
    return;
}   

/**
 * @brief tmc5041_start is called when IO_START ioctl is applied to the driver.
 * It should be removed at some point, it just starts turning without any security features.
 * DANGER
 * Also can be called by tmc5041_loop, but this one should be removed too.
 * Anyways, no process should be waiting on this code, so it's not going to wake up anything.
 * @param chip
 * @return
 */
static int tmc5041_start(struct tmc5041* chip) {
    uint8_t status;
    uint32_t response;
    uint32_t setting = 0;
    int ret;
    struct spi_device* spi = chip->spi_device;

    printk(KERN_INFO "TMC5041 started \n");

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered");
        chip->hardware.hw_status = NO_POWER_ERROR;
        return -EINVAL;
    }

    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_SPEED : FIRST_CHANNEL_SPEED;
    ret = tmc5041_spi_fdx_transfer(spi, VMAX, setting, &status, &response, TMC5041_READ); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VMAX\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }

    dev_info(&spi->dev, "VMAX");
    ret = tmc5041_spi_fdx_transfer(spi, RAMPMODE, MODE_VELPOS, &status, &response, TMC5041_READ); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up RAMPMODE\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    dev_info(&spi->dev, "RAMPMODE");

    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_CHOPCONF : FIRST_CHANNEL_CHOPCONF;
    //setting = chip->hardware.using_second_channel ? 0x000300C5 : 0x020100C5;
    ret = tmc5041_spi_fdx_transfer(spi, CHOPCONF,   setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up CHOPCONF\n");
        return ret;
    }
    dev_info(&spi->dev, "CHOPCONF");
    // IHOLD = 9, IRUN = 26, IHOLDDELAY = 1
    // or IHOLD = 9, IRUN = 16, IHOLDDELAY = 1 if using the second channel.
    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_IHOLDRUN : FIRST_CHANNEL_IHOLDRUN;
    //setting = chip->hardware.using_second_channel ? 0x00011009 : 0x00011A09;
    ret = tmc5041_spi_fdx_transfer(spi, IHOLDRUN,   setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up IHOLDRUN\n");
        return ret;
    }
    dev_info(&spi->dev, "IHOLDRUN");

    ret = tmc5041_spi_fdx_transfer(spi, TZEROWAIT,  TZEROWAIT_CONTENT, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up TZEROWAIT\n");
        return ret;
    }

    dev_info(&spi->dev, "TZEROWAIT");
    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_PWMCONF : FIRST_CHANNEL_PWMCONF;
    //setting = chip->hardware.using_second_channel ? 0x000504C8 : 0x000401C8;
    ret = tmc5041_spi_fdx_transfer(spi, PWMCONF,    setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up PWMCONF\n");
        return ret;
    }

    dev_info(&spi->dev, "PWMCONF");
    ret = tmc5041_spi_fdx_transfer(spi, VHIGH,      VHIGH_CONTENT, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VHIGH\n");
        return ret;
    }

    dev_info(&spi->dev, "VHIGH");
    ret = tmc5041_spi_fdx_transfer(spi, VCOOLTHRS,  VCOOLTHRS_CONTENT, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VCOOLTHRS\n");
        return ret;
    }

    dev_info(&spi->dev, "VCOOLTHRS");
    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, AMAX,       setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up AMAX\n");
        return ret;
    }

    dev_info(&spi->dev, "AMAX");
    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, DMAX,       setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up DMAX\n");
        return ret;
    }

    dev_info(&spi->dev, "DMAX");
    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, A1,         setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up A1\n");
        return ret;
    }

    dev_info(&spi->dev, "A1");
    //setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, D1,         setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up D1\n");
        return ret;
    }

    dev_info(&spi->dev, "D1");
    ret = tmc5041_spi_fdx_transfer(spi, VSTART,     VSTART_CONTENT, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VSTART\n");
        return ret;
    }

    dev_info(&spi->dev, "VSTART");
    ret = tmc5041_spi_fdx_transfer(spi, VSTOP,      VSTOP_CONTENT, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VSTOP\n");
        return ret;
    }
    
    dev_info(&spi->dev, "VSTOP");
    ret = tmc5041_spi_fdx_transfer(spi, V1,         V1_CONTENT, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up V1\n");
        return ret;
    }

    dev_info(&spi->dev, "V1");
    ret = tmc5041_spi_fdx_transfer(spi, GCONF, setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up V1\n");
        return ret;
    }
    dev_info(&spi->dev, "GCONF");
    ret = tmc5041_spi_fdx_transfer(spi, GSTAT, setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up V1\n");
        return ret;
    }
    dev_info(&spi->dev, "GSTAT");
    ret = tmc5041_spi_fdx_transfer(spi, DRV_STATUS, setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up V1\n");
        return ret;
    }
    dev_info(&spi->dev, "DRV_STATUS");
    ret = tmc5041_spi_fdx_transfer(spi, DRV_STATUS, setting, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up V1\n");
        return ret;
    }
    
    return 0;
}

/**
 * @brief tmc5041_stop can be called with IO_STOP ioctl, and will return directly to the caller.
 * No process should sleep on this method, so it will not wake anything up.
 * Should be kept as a safeguard.
 * @param chip
 * @return
 */
static int tmc5041_stop(struct tmc5041* chip) {
    uint8_t status;
    uint32_t response;
    int ret;
    struct spi_device* spi = chip->spi_device;

    printk(KERN_INFO "TMC5041 stopped \n");

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered");
        chip->hardware.hw_status = NO_POWER_ERROR;
        return -EINVAL;
    }

    ret = tmc5041_spi_fdx_transfer(spi, VMAX, NO_SPEED, &status, &response, TMC5041_WRITE); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VMAX\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }
    ret = tmc5041_spi_fdx_transfer(spi, RAMPMODE, MODE_VELPOS, &status, &response, TMC5041_WRITE); // DEPENDS ON MOTOR
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up RAMPMODE\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        return ret;
    }

    chip->hardware.hw_status = OPERATION_COMPLETE;
    
//    chip->hardware.looping = false;
    
    return 0;
}

/**
 * @brief tmc5041_loop starts the motor in one direction and schedules a task that will change this direction.
 * It is mostly used for demonstration purposes, and should be removed in the final version.
 * No process should be sleeping on this method, so it will not wake anything up. Neither will the scheduled task.
 * @param chip
 * @return
 */
/*
static int tmc5041_loop(struct tmc5041* chip) {
    uint8_t status;
    uint32_t response;
    int ret;
    struct spi_device* spi = chip->spi_device;

    printk(KERN_INFO "TMC5041 loop \n");

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered");
        chip->hardware.hw_status = NO_POWER_ERROR;
        return -EINVAL;
    }
    
    ret = tmc5041_setup_limit_switches(chip);
    if (ret < 0)
        return ret;
    
    chip->hardware.ramp_mode = MODE_VELPOS;
    
    ret = tmc5041_start(chip);
    if (ret < 0)
        return ret;
    
    // We now have started moving towards the entrance
    // Let's schedule a change of direction 
    
    chip->hardware.looping = true;
    queue_delayed_work(chip->tmc5041_wq, &chip->change_direction_work, POLL_1S);
    
    return 0;
}
*/

/**
 * @brief tmc5041_change_direction_work should be scheduled from tmc5041_loop.
 * No process should be waiting on this one, so no wake up will be made.
 * @param work_struct_ptr
 */
/*
static void tmc5041_change_direction_work(struct work_struct *work_struct_ptr) {
    uint8_t status;
    uint32_t response;
    uint32_t command;
    int ret;
    
    printk(KERN_INFO "TMC5041 change direction work\n");

    struct delayed_work* delayed = container_of(work_struct_ptr, struct delayed_work, work);
    struct tmc5041* chip = container_of(delayed, struct tmc5041, change_direction_work);
    struct spi_device* spi = chip->spi_device;
    
    if (!chip->hardware.looping) {
        dev_err(&spi->dev, "Change direction work called but the motor isn't looping\n");
        chip->hardware.hw_status = LOOP_BAD_CONFIG_ERROR;
        return;
    }
    
    if (!chip->powered) {
        dev_err(&spi->dev, "TMC5041 is not powered\n");
        chip->hardware.hw_status = NO_POWER_ERROR;
        goto signal_status;
    }
    
    switch(chip->hardware.ramp_mode) {
    case MODE_VELPOS:
        chip->hardware.ramp_mode = MODE_VELNEG;
        break;
    case MODE_VELNEG:
        chip->hardware.ramp_mode = MODE_VELPOS;
        break;
    default:
        dev_err(&spi->dev, "Unexpected ramp mode, stopping motor\n");
        chip->hardware.hw_status = LOOP_BAD_CONFIG_ERROR;
        tmc5041_stop(chip);
        goto signal_status;
        break;
    }
    
    ret = tmc5041_spi_fdx_transfer(spi, RAMPMODE, chip->hardware.ramp_mode, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up RAMPMODE\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto signal_status;
    }
    
    queue_delayed_work(chip->tmc5041_wq, &chip->change_direction_work, POLL_1S);
    
signal_status:
    return;
}
*/

/**
 * @brief tmc5041_register_check_work is a generic work that can be used to poll a register value at
 * regular intervals. Methods relying on this work should provide two things :
 * - the register to poll, il chip->hardware.register_to_check
 * - the callback to pass this register to, in chip->hardware.check_callback
 * - whether to wake up on error / success, in chip->hardware.register_check_require_wakeup
 * @param work_struct_ptr
 */
static void tmc5041_register_check_work(struct work_struct *work_struct_ptr) {
    uint8_t status = 0;
    uint32_t response = 0;
    uint32_t command = 0;
    int ret = 0;
    int poll_delay = 0;

    printk(KERN_INFO "TMC5041 check register work\n");

    struct delayed_work* delayed = container_of(work_struct_ptr, struct delayed_work, work);
    struct tmc5041* chip = container_of(delayed, struct tmc5041, register_check_work);
    struct spi_device* spi = chip->spi_device;

    if (UNKNOWN_REGISTER == chip->hardware.register_to_check || NULL == chip->hardware.check_callback) {
        dev_err(&spi->dev, "No register to check, or callback invalid\n");
        chip->hardware.hw_status = REGISTER_CHECK_CONFIG_ERROR;
        goto _wake_up_and_return;
    }

// Start by checking the switches status
    ret = tmc5041_spi_fdx_transfer(spi, RAMP_STAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto _wake_up_and_return;
    }
    
    // This command must be sent twice because the chip doesn't send back answers on the first call.
    ret = tmc5041_spi_fdx_transfer(spi, GSTAT, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto _wake_up_and_return;
    }
        
    if (FIELD_GET(response, TMC5041_STATUS_STOP_R_MASK, TMC5041_STATUS_STOP_R_SHIFT))
        dev_info(&spi->dev, "Right switch active\n");
    else if (FIELD_GET(response, TMC5041_STATUS_STOP_L_MASK, TMC5041_STATUS_STOP_L_SHIFT))
        dev_info(&spi->dev, "Left switch active\n");
    
    dev_info(&spi->dev, "Displaying GSTAT content\n");
    ret = tmc5041_spi_fdx_transfer(spi, DRV_STATUS, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto _wake_up_and_return;
    }
// then continue doing whatever

    dev_info(&spi->dev, "Checking register 0x%x\n", chip->hardware.register_to_check);
    dev_info(&spi->dev, "Displaying DRV_STATUS content\n");
    
    ret = tmc5041_spi_fdx_transfer(spi, chip->hardware.register_to_check, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't read in check register - 1\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto _wake_up_and_return;
    }

    ret = tmc5041_spi_fdx_transfer(spi, chip->hardware.register_to_check, command, &status, &response, TMC5041_READ);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't read in check register - 2\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto _wake_up_and_return;
    }
    
    chip->hardware.register_value = response;

    ret = chip->hardware.check_callback(chip, &poll_delay); // The callback should set hw_status to what it wants to be read later.
    if (ret < 0) {
        dev_err(&spi->dev, "Error in check callback\n");
        chip->hardware.hw_status = COMMUNICATION_ERROR;
        goto _wake_up_and_return;
    }
    
    if (0 < poll_delay) {
        dev_info(&spi->dev, "Scheduling again !\n");
        queue_delayed_work(chip->tmc5041_wq, &chip->register_check_work, poll_delay);
        return;
    } else {
        dev_info(&spi->dev, "No rescheduling, callback has set its status code\n");
    }


// TO-DO    IF STATUS IS TARGET EXCEEDED STOP MOTOR.

_wake_up_and_return:
    // AND wake up any waiting process here !!
    // And the next time the file is read, the hw_status will be whatever was set by the last instruction before this block.
    if (chip->hardware.register_check_require_wakeup) {
        dev_info(&spi->dev, "Waking up sleeping processes\n");
        wake_up_interruptible(&chip->data_ready_queue);
    }
    return;
}

/**
 * @brief tmc5041_position_check_callback checks the RAMP_STAT register for the POSITION_REACHED bit.
 * If the bit is set, the method returns with a success and no poll_delay, meaning that register_check_work will not reschedule.
 * If the bit is not set, it checks how many times this poll was made. If the count exceeds MAX_POSITION_CHECKS, we don't reschedule,
 * but the hw_status is set accordingly. Otherwise, it just sets up another second poll delay.
 * @param chip
 * @param poll_delay
 * @return
 */
static int tmc5041_position_check_callback(struct tmc5041* chip, int* poll_delay) {
    if (OPERATION_COMPLETE == chip->hardware.hw_status) {
        // The operation was stopped manually. No need to continue polling.
        dev_info(&chip->spi_device->dev, "Operation stopped manually. Not polling anymore\n");
        *poll_delay = 0;
        return 0;
    }
    int position_reached = FIELD_GET(chip->hardware.register_value, TMC5041_POSITION_REACHED_MASK, TMC5041_POSITION_REACHED_SHIFT);
    if (1 == position_reached) {
        // Position reached, we're good !
        chip->hardware.hw_status = TARGET_POSITION_REACHED;
        *poll_delay = 0;
        dev_info(&chip->spi_device->dev, "Position reached !\n");
        return 0;
    } else {
        // Position not reached. How many times have we checked ?
        if (chip->hardware.position_checks_counter < MAX_POSITION_CHECKS) {
            dev_info(&chip->spi_device->dev, "Polling position one more time\n");
            // We can poll at least one more time
            chip->hardware.position_checks_counter++;
            *poll_delay = POLL_1S;
            return 0;
        } else {
            // We have already polled too many times.
            dev_info(&chip->spi_device->dev, "Position checked too many times !\n");
            chip->hardware.hw_status = POSITION_CHECK_COUNT_EXCEEDED_ERROR;
            *poll_delay = 0;
            return 0;
        }
    }
}

static int tmc5041_setup(struct tmc5041* chip) {
    uint8_t status;
    uint32_t response;
    uint32_t setting;
    int ret;

    if (!chip->powered) {
        dev_err(&chip->spi_device->dev, "TMC5041 is not powered\n");
        return -EINVAL;
    }

    struct spi_device* spi = chip->spi_device;
    // VSENSE = 0 for channel 1, VSENSE = 1 for channel 2.
    // MRES = 2 for channel 1, MRES = 0 for channel 2.

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_CHOPCONF : FIRST_CHANNEL_CHOPCONF;
    //setting = chip->hardware.using_second_channel ? 0x000300C5 : 0x020100C5;
    ret = tmc5041_spi_fdx_transfer(spi, CHOPCONF,   setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up CHOPCONF\n");
        return ret;
    }
    // IHOLD = 9, IRUN = 26, IHOLDDELAY = 1
    // or IHOLD = 9, IRUN = 16, IHOLDDELAY = 1 if using the second channel.
    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_IHOLDRUN : FIRST_CHANNEL_IHOLDRUN;
    //setting = chip->hardware.using_second_channel ? 0x00011009 : 0x00011A09;
    ret = tmc5041_spi_fdx_transfer(spi, IHOLDRUN,   setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up IHOLDRUN\n");
        return ret;
    }

    ret = tmc5041_spi_fdx_transfer(spi, TZEROWAIT,  TZEROWAIT_CONTENT, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up TZEROWAIT\n");
        return ret;
    }

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_PWMCONF : FIRST_CHANNEL_PWMCONF;
    //setting = chip->hardware.using_second_channel ? 0x000504C8 : 0x000401C8;
    ret = tmc5041_spi_fdx_transfer(spi, PWMCONF,    setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up PWMCONF\n");
        return ret;
    }

    ret = tmc5041_spi_fdx_transfer(spi, VHIGH,      VHIGH_CONTENT, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VHIGH\n");
        return ret;
    }

    ret = tmc5041_spi_fdx_transfer(spi, VCOOLTHRS,  VCOOLTHRS_CONTENT, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VCOOLTHRS\n");
        return ret;
    }

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, AMAX,       setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up AMAX\n");
        return ret;
    }

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, DMAX,       setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up DMAX\n");
        return ret;
    }

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, A1,         setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up A1\n");
        return ret;
    }

    setting = chip->hardware.using_second_channel ? SECOND_CHANNEL_ACCEL : FIRST_CHANNEL_ACCEL;
    ret = tmc5041_spi_fdx_transfer(spi, D1,         setting, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up D1\n");
        return ret;
    }

    ret = tmc5041_spi_fdx_transfer(spi, VSTART,     VSTART_CONTENT, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VSTART\n");
        return ret;
    }

    ret = tmc5041_spi_fdx_transfer(spi, VSTOP,      VSTOP_CONTENT, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up VSTOP\n");
        return ret;
    }
    
    ret = tmc5041_spi_fdx_transfer(spi, V1,         V1_CONTENT, &status, &response, TMC5041_WRITE);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up V1\n");
        return ret;
    }
    
    ret = tmc5041_setup_limit_switches(chip);
    if (ret < 0) {
        dev_err(&spi->dev, "Couldn't set up limit switches\n");
        return ret;
    }

//    tmc5041_spi_fdx_transfer(spi, CHOPCONF, 0x000100C5, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, IHOLDRUN, 0x00010A05, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, TZEROWAIT, 10000, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, PWMCONF, 0x000401C8, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, VHIGH, 400000, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, VCOOLTHRS, 30000, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, AMAX, 100000, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, VMAX, 100000, &status, &response, true);
//    tmc5041_spi_fdx_transfer(spi, RAMPMODE, 1, &status, &response, true);

    return 0;
}

// ----- file operations ----- //

/**
 * @brief tmc5041_open
 * @return
 */
static int tmc5041_fopen(struct inode* node, struct file* file) {
    printk(KERN_INFO "Device file opened\n");
    return 0;
}

/**
 * @brief tmc5041_release
 * @return
 */
static int tmc5041_frelease(struct inode* node, struct file* file) {
    printk(KERN_INFO "Device file closed \n");
    return 0;
}

static unsigned int tmc5041_fpoll(struct file* file, poll_table* poll_wait_table) {
    struct tmc5041* chip = to_tmc5041_priv(file);
    unsigned int mask = 0;

    poll_wait(file, &chip->data_ready_queue, poll_wait_table);

    // If data becomes available, wake up any process waiting on poll with
    // wake_up_interruptible(&chip->data_ready_queue);
    /* Typically, the polls that happen on 
     * tmc5041_is_home_poll_work
     * tmc5041_register_check_work
     * could wake up the process, if the information is relevant.
     * Alternatively, if we set up the IRQ, then the IRQ handler should wake up any
     * process in the queue.
     */

    /* List of status where something will wake up :
     * - TARGET_IN_PROGRESS => TARGET_POSITION_REACHED
     * - HOMING_IN_PROGRESS, homing_procedure_active => HOMING_COMPLETE
     *
     */
    // Basically, if we're doing a homing or a target operation, wait until the corresponding
    // poll worker wakes the process up.
    // Otherwise, just return that data can be read.
    
    switch (chip->hardware.hw_status) {
    case HOMING_IN_PROGRESS:
    case TARGET_IN_PROGRESS:
        dev_info(&chip->spi_device->dev, "Setting readable mask to 0, waiting on homing or target...\n");
        mask = 0;
        break;
    default:
        dev_info(&chip->spi_device->dev, "No operation in progress, can read.\n");
        mask |= POLLIN | POLLRDNORM;
        break;
    }
    
    return mask;
}

/**
 * @brief tmc5041_read
 * @return
 */
static int tmc5041_fread(struct file* file, char __user *buf, size_t size, loff_t* off) {
    printk(KERN_INFO "Read\n");
    return 0;
}

/**
 * @brief tmc5041_write
 * @return
 */
static int tmc5041_fwrite(struct file* file, const char* buf, size_t size, loff_t* off) {
    printk(KERN_INFO, "Write\n");
    return 0;
}

/**
 * @brief tmc5041_ioctl
 * @param file
 * @param cmd
 * @param arg
 * @return
 */
static long tmc5041_fioctl(struct file *file, unsigned int cmd, unsigned long arg) {
    int int_value = 0;
    int ret;

    struct tmc5041* chip = to_tmc5041_priv(file);
    struct spi_device* spi = chip->spi_device;

    dev_info(&spi->dev, "Arg pointer : %p\n", (void __user *) arg);

    switch (cmd) {
    case IOW_TARGET:
        chip->hardware.hw_status = TARGET_COMMAND_ISSUED;
        ret = get_user(int_value, (int __user *) arg);
	    dev_info(&spi->dev, "Value passed : %i\n", int_value);
        if (ret < 0)
            break;
        ret = tmc5041_w_target(int_value, chip);
        break;
    case IOR_POSITION:
        chip->hardware.hw_status = POSITION_READ_COMMAND_ISSUED;
        ret = tmc5041_r_position(&int_value, chip);
        if (ret < 0)
            break;
        ret = put_user(int_value, (int __user *) arg);
        break;
    case IO_HOME:
        chip->hardware.hw_status = HOME_COMMAND_ISSUED;
        ret = tmc5041_home(chip);
        break;
    case IO_STOP:
        chip->hardware.hw_status = STOP_COMMAND_ISSUED;
        ret = tmc5041_stop(chip);
        break;
    case IO_START:
        chip->hardware.hw_status = START_COMMAND_ISSUED;
        ret = tmc5041_start(chip);
        break;
//    case IO_LOOP:
//        chip->hardware.hw_status = LOOP_COMMAND_ISSUED;
//        ret = tmc5041_loop(chip);
//        break;
    case IOR_STATUS:
        ret = put_user(chip->hardware.hw_status, (int __user *) arg);
        break;
    case IOW_SWAP_CHANNEL:
        ret = get_user(int_value, (int __user *) arg);
        dev_info(&spi->dev, "Value passed : %i\n", int_value);
        if (ret < 0)
            break;
        chip->hardware.using_second_channel = int_value;
        break;
    default:
        return -ENOTTY;
    }
    return ret;
}

/**
 * @brief to_tmc5041_priv inspired from pxa3xx-gcu.c
 * @param file
 * @return
 */
static inline struct tmc5041* to_tmc5041_priv(struct file* file) {
    struct miscdevice *mdev = file->private_data;
    return container_of(mdev, struct tmc5041, misc_device);
}

static struct file_operations tmc5041_fops = {
    .owner = THIS_MODULE,
    .read = tmc5041_fread,
    .write = tmc5041_fwrite,
    .unlocked_ioctl = tmc5041_fioctl,
    .open = tmc5041_fopen,
    .poll = tmc5041_fpoll,
    .release = tmc5041_frelease,
};

// ***** power management ***** //
static int tmc5041_set_power(struct tmc5041* chip, bool powered) {
    chip->powered = powered;
//    gpiod_set_value_cansleep(chip->gpio_power, chip->powered);
    gpiod_set_value_cansleep(chip->gpio_enable, chip->powered);
    return 0;
}

static int tmc5041_get_power(struct tmc5041* chip) {
    chip->powered = /* gpiod_get_value_cansleep(chip->gpio_power) & */ gpiod_get_value_cansleep(chip->gpio_enable);

    return chip->powered;
}

// ***** SYSFS ***** //
static int tmc5041_sysfs_get_power(struct device *dev,
                             struct device_attribute *attr,
                             char *buf) {
    struct spi_device* client = to_spi_device(dev);
    struct tmc5041 *chip = spi_get_drvdata(client);

    dev_info(dev, "Sysfs get power");

    tmc5041_get_power(chip);

    return sprintf(buf, "%i\n", chip->powered ? 1 : 0);
}

static int tmc5041_sysfs_set_power(struct device *dev,
                             struct device_attribute *attr,
                             const char *buf,
                             size_t size) {

    struct spi_device* client = to_spi_device(dev);
    struct tmc5041 *data = spi_get_drvdata(client);

    dev_info(dev, "Sysfs set power");

    int ret;
    unsigned long value;
    char *power_on = "on";
    char *power_off = "off";

    ret = kstrtoul(buf, 0, &value);

    /* we use count-1 as echo adds \n to the terminal string */
    if (!strncmp(buf, power_on, size-1) | (value == 0x01)) {
        tmc5041_set_power(data, true);
    } else if (!strncmp(buf, power_off, size-1) | (value == 0x00)) {
        tmc5041_set_power(data, false);
    } else {
        dev_err(dev, "Bad control string");
        return -EINVAL;
    }

    return size;
}

static DEVICE_ATTR(pm, S_IWUSR | S_IRUGO, tmc5041_sysfs_get_power, tmc5041_sysfs_set_power);

static int tmc5041_sysfs_register(struct device *dev) {
    return device_create_file(dev, &dev_attr_pm);
}

static void tmc5041_sysfs_unregister(struct device *dev) {
    device_remove_file(dev, &dev_attr_pm);
}

// ***** driver management ***** //

static int tmc5041_probe(struct spi_device* client) {
    int err;
    struct tmc5041* chip;

    dev_info(&client->dev, "Probing TMC5041 driver with version %s.\n", DRIVER_VERSION);

    /* we allocate the misc device structure as part of our own allocation,
     * so we can get a pointer to our priv structure later on with
     * container_of(). source : pxa3xx-gcu.c */
    chip = devm_kzalloc(&client->dev, sizeof(struct tmc5041), GFP_KERNEL);
    if (!chip) {
        dev_err(&client->dev, "%s: unable to allocate memory\n", __func__);
        return -ENOMEM;
    }

    /* Getting the power management gpios. If they aren't available yet, defer. */
//    chip->gpio_power = devm_gpiod_get(&client->dev, "power", GPIOD_ASIS);
//    if (IS_ERR(chip->gpio_power)) {
//        if (PTR_ERR(chip->gpio_power) == -EPROBE_DEFER) {
//            dev_info(&client->dev, "Probe defer on gpio_power");
//            return -EPROBE_DEFER;
//        }
//        err = PTR_ERR(chip->gpio_power);
//        dev_err(&client->dev, "Unable to claim gpio \"power\", ptr_err %ld\n", err);
//        return err;
//    }

//    err = gpiod_direction_output(chip->gpio_power, true);
//    if (err < 0) {
//	dev_err(&client->dev, "Unable to set gpio power as output\n");
//	return err;
//    }

//    gpiod_set_value_cansleep(chip->gpio_power, true);

    chip->gpio_enable = devm_gpiod_get(&client->dev, "enable", GPIOD_ASIS);
    if (IS_ERR(chip->gpio_enable)) {
        if (PTR_ERR(chip->gpio_enable) == -EPROBE_DEFER) {
            dev_info(&client->dev, "Probe defer on gpio_enable");
            return -EPROBE_DEFER;
        }
        err = PTR_ERR(chip->gpio_enable);
        dev_err(&client->dev, "Unable to claim gpio \"enable\", ptr_err %ld\n", err);
        return err;
    }

    err = gpiod_direction_output(chip->gpio_enable, true);
    if (err < 0) {
	dev_err(&client->dev, "Unable to set gpio enable as output\n");
	return err;
    }

    gpiod_set_value_cansleep(chip->gpio_enable, true);

    /* setting up the misc interface */
    chip->misc_device.minor = MISC_DYNAMIC_MINOR;
    chip->misc_device.name = MISC_NAME;
    chip->misc_device.fops = &tmc5041_fops;

    /* If we need to parse some more information from the DT, it can be done here. */
    /*
    if (client->dev.of_node) {
        dev_info(&client->dev, "Parsing DT\n");
        chip->platform_data = tmc5041_parse_dt(client);
    }

    if (!chip->platform_data) {
        dev_err(&client->dev, "%s: unable to parse device tree\n", __func__);
        return -EFAULT;
    }
    */

    /* setting up the sysfs interface */
    err = tmc5041_sysfs_register(&client->dev);
    if (err) {
        dev_err(&client->dev, "Unable to create sysfs entry for TMC5041 power management");
        return err;
    }

    /* setting up the SPI parameters */
    client->mode = SPI_MODE_3;
    client->bits_per_word = 8;
    err = spi_setup(client);

    if (err < 0) {
        dev_err(&client->dev, "Error while setting up SPI\n");
        return err;
    }

    /* Storing the pointers to and from the chip
     * spi_device in one another to access them easily */
    chip->spi_device = client;
    spi_set_drvdata(client, chip);

    /* Setting up the tmc5041 only makes sense if the driver is powered on.
     * So, power it on, wait a few milliseconds, then set it up. */

    err = tmc5041_set_power(chip, true);
    usleep_range(10000,20000); // TODO CHECK if it is enough.

    /* This is the setup for the first channel */
    err = tmc5041_setup(chip);
    if (err < 0) {
        dev_err(&client->dev, "Error while setting up TMC5041 motor driver\n");
        return err;
    }

    /* Now the setup for the second channel */
    chip->hardware.using_second_channel = true;
    err = tmc5041_setup(chip);
    if (err < 0) {
        dev_err(&client->dev, "Error while setting up TMC5041 motor driver\n");
        return err;
    }

    chip->hardware.using_second_channel = false;

    /* Setting up the miscellaneous character device that will allow userspace
     * to communicate with the driver and send it commands. */
    err = misc_register(&chip->misc_device);
    if (err < 0) {
        dev_err(&client->dev, "Could not register to misc framework\n");
        return err;
    }

    dev_info(&client->dev, "TMC5041 got minor %i\n", chip->misc_device.minor);
    
    // https://stackoverflow.com/questions/37149212/calling-spi-write-periodically-in-a-linux-driver
    chip->tmc5041_wq = create_singlethread_workqueue("tmc5041_workqueue");
    INIT_DELAYED_WORK(&chip->homing_check_work, tmc5041_is_home_poll_work);
//    INIT_DELAYED_WORK(&chip->change_direction_work, tmc5041_change_direction_work);
    INIT_DELAYED_WORK(&chip->register_check_work, tmc5041_register_check_work);
    
    /* Initializing the queue in which the polling process can be put to sleep */
    init_waitqueue_head(&chip->data_ready_queue);
    
    chip->hardware.homing_procedure_active = false;
    chip->hardware.homing_procedure_complete = false;
    chip->hardware.limit_switches_set_up = false;
    chip->hardware.homing_checks_counter = 0;
    chip->hardware.position_checks_counter = 0;
    chip->hardware.hw_status = NO_ERROR;
//    chip->hardware.looping = false;
    chip->hardware.ramp_mode = MODE_POSITION;
    chip->hardware.register_to_check = UNKNOWN_REGISTER;
    chip->hardware.register_value = 0;
    chip->hardware.check_callback = NULL;
    chip->hardware.register_check_require_wakeup = false;
    chip->hardware.using_second_channel = false;

    return 0;
}

static int tmc5041_remove(struct spi_device* client) {
    dev_info(&client->dev, "Deregistering misc device\n");
    struct tmc5041* chip = spi_get_drvdata(client);
    flush_workqueue(chip->tmc5041_wq);
    destroy_workqueue(chip->tmc5041_wq);
    misc_deregister(&chip->misc_device);
    tmc5041_sysfs_unregister(&client->dev);
    return 0;
}

static const struct spi_device_id tmc5041_spi_match[] = {
    {"tmc5041-spi"},
    { /* sentinel */ },
};
MODULE_DEVICE_TABLE(spi, tmc5041_spi_match);

static const struct of_device_id tmc5041_of_match[] = {
    {.compatible = "trinamic,tmc5041-spi" },
    { /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, tmc5041_of_match);

static struct spi_driver tmc5041_driver = {
    .id_table = tmc5041_spi_match,
    .probe = tmc5041_probe,
    .remove = tmc5041_remove,
    .driver = {
        .name = "tmc5041-spi",
        .owner = THIS_MODULE,
        .of_match_table = tmc5041_of_match,
    },
};

module_spi_driver(tmc5041_driver);

//static int __init tmc5041_driver_init(void) {
//    return spi_add_driver(&tmc5041_driver);
//}
//late_initcall(tmc5041_driver_init);

//static void __exit tmc5041_driver_exit(void) {
//    return spi_del_driver(&tmc5041_driver);
//}
//module_exit(tmc5041_driver_exit);

MODULE_DESCRIPTION("Driver for Trinamic TMC5041 stepper motor driver");
MODULE_AUTHOR("Damien Kirk");
MODULE_LICENSE("GPL");
MODULE_ALIAS("spi:tmc5041");
